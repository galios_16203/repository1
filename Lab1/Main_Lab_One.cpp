#include "Header_Lab_One.h"
using namespace std;

int main(int argc, char *argv[])
{
	try
	{
		int n = 2, m = 2;
		vector<string> words;
		vector<pair<int, string>> sorted;
		map<string, int> phrases;

		switch (argc)
		{
		case 2:
		{
			if (string(argv[1]) == "-")
			{
				cout << "Write string:" << endl;
				words = get_vector(cin);
			}
			else
			{
				ifstream ist(argv[1]);
				if (!ist)
				{
					throw string(argv[1]);
				}
				words = get_vector(ist);
			}
			phrases = get_phrases(n, words);
			sorted = sort_phrases(m, phrases);

			break;
		}
		case 4:
		{
			if (string(argv[1]) == "-n")
			{
				n = stoi(string(argv[2]));
			}
			else if (string(argv[1]) == "-m")
			{
				m = stoi(string(argv[2]));
			}
			else
			{
				throw 1;
			}

			if (string(argv[3]) == "-")
			{
				cout << "Write string:" << endl;
				words = get_vector(cin);
			}
			else
			{
				ifstream ist(argv[3]);
				if (!ist)
				{
					throw string(argv[3]);
				}
				words = get_vector(ist);
			}
			phrases = get_phrases(n, words);
			sorted = sort_phrases(m, phrases);

			break;
		}
		case 6:
		{
			if (string(argv[1]) == "-n")
			{
				n = stoi(string(argv[2]));
				if (string(argv[3]) == "-m")
				{
					m = stoi(string(argv[4]));
				}
				else
				{
					throw 3;
				}
			}
			else if (string(argv[1]) == "-m")
			{
				m = stoi(string(argv[2]));
				if (string(argv[3]) == "-n")
				{
					n = stoi(string(argv[4]));
				}
				else
				{
					throw 3;
				}
			}
			else
			{
				throw 1;
			}

			if (string(argv[5]) == "-")
			{
				cout << "Write string:" << endl;
				words = get_vector(cin);
			}
			else
			{
				ifstream ist(argv[5]);
				if (!ist)
				{
					throw string(argv[5]);
				}
				words = get_vector(ist);
			}
			phrases = get_phrases(n, words);
			sorted = sort_phrases(m, phrases);

			break;
		}
		default:
		{
			throw 1;
			break;
		}
		}
		for (auto i : sorted)
		{
			cout << i.second << " (" << i.first << ")" << endl;
		}
		cin.get();
	}
	catch(int ex)
	{
		if ((ex > 0) && (ex < argc))
		{
			cout << "Unknown option " << argv[ex] << endl;
			cin.get();
		}
	}
	catch (string str)
	{
		cout << "Can't open the file " << str << endl;
		cin.get();
	}
	return 0;
}