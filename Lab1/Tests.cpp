#define CATCH_CONFIG_RUNNER 
#include "Header_Lab_One.h" 
#include "catch.h" 
using namespace std;

TEST_CASE("Gets vector from istream", "[get_vector]")
{
	stringstream ist1("1 1 1 1");
	vector<string> words1{ "1", "1", "1", "1" };

	stringstream ist2("");
	vector<string> words2 = { };

	stringstream ist3("1");
	vector<string> words3{ "1" };

	stringstream ist4("1 12 123 1234");
	vector<string> words4{ "1", "12", "123", "1234" };

	REQUIRE(get_vector(ist1) == words1);
	REQUIRE(get_vector(ist2) == words2);
	REQUIRE(get_vector(ist3) == words3);
	REQUIRE(get_vector(ist4) == words4);
}

TEST_CASE("Gets map from vector", "[get_phrases]")
{
	int n1 = 2;
	int n2 = 3;
	int n3 = 4;
	vector<string> words1{ "1", "1", "1", "1" };
	vector<string> words2 = {};
	vector<string> words3{ "1" };
	vector<string> words4{ "1", "12", "123", "1234" };

	map<string, int> phrases11;
	map<string, int> phrases21;
	map<string, int> phrases31;
	map<string, int> phrasesemp;
	map<string, int> phrases14;
	map<string, int> phrases24;
	map<string, int> phrases34;

	phrases11["1 1"] = 3;
	phrases21["1 1 1"] = 2;
	phrases31["1 1 1 1"] = 1;
	phrases14["1 12"] = 1;
	phrases14["12 123"] = 1;
	phrases14["123 1234"] = 1;
	phrases24["1 12 123"] = 1;
	phrases24["12 123 1234"] = 1;
	phrases34["1 12 123 1234"] = 1;


	REQUIRE(get_phrases(n1, words1) == phrases11);
	REQUIRE(get_phrases(n2, words1) == phrases21);
	REQUIRE(get_phrases(n3, words1) == phrases31);
	REQUIRE(get_phrases(n1, words2) == phrasesemp);
	REQUIRE(get_phrases(n2, words2) == phrasesemp);
	REQUIRE(get_phrases(n3, words2) == phrasesemp);
	REQUIRE(get_phrases(n1, words3) == phrasesemp);
	REQUIRE(get_phrases(n2, words3) == phrasesemp);
	REQUIRE(get_phrases(n3, words3) == phrasesemp);
	REQUIRE(get_phrases(n1, words4) == phrases14);
	REQUIRE(get_phrases(n2, words4) == phrases24);
	REQUIRE(get_phrases(n3, words4) == phrases34);
}

TEST_CASE("Sorts phrases", "[sort_phrases]")
{
	int m1 = 2;
	int m2 = 3;
	int m3 = 4;
	int m = 10;

	map<string, int> phrases;
	map<string, int> phrasesemp;

	phrases["1 1"] = 2;
	phrases["2 1"] = 3;
	phrases["1 2"] = 4;

	vector<pair<int, string>> sorted1;
	vector<pair<int, string>> sorted2; 
	vector<pair<int, string>> sorted3; 
	vector<pair<int, string>> sortedemp;

	sorted1.push_back(make_pair(4, "1 2"));
	sorted1.push_back(make_pair(3, "2 1"));
	sorted1.push_back(make_pair(2, "1 1"));
	sorted2.push_back(make_pair(4, "1 2"));
	sorted2.push_back(make_pair(3, "2 1"));
	sorted3.push_back(make_pair(4, "1 2"));

	REQUIRE(sort_phrases(m1, phrases) == sorted1);
	REQUIRE(sort_phrases(m2, phrases) == sorted2);
	REQUIRE(sort_phrases(m3, phrases) == sorted3);
	REQUIRE(sort_phrases(m, phrases) == sortedemp);
	REQUIRE(sort_phrases(m1, phrasesemp) == sortedemp);
}

int main(int argc, char* const argv[])
{
	int result = Catch::Session().run(argc, argv);
	cin.get();
	return result;
}