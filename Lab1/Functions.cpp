#include "Header_Lab_One.h"
using namespace std;

vector<string> get_vector(istream& ist)
{
	vector<string> words;
	for (string word; ist >> word; )
	{
		words.push_back(word);
	}
	return words;
}

map<string, int> get_phrases(int n, const vector<string>& words)
{
	string phrase;
	map<string, int> phrases;
	int size = words.size();

	for (int i = 0; (i + n) <= size; ++i)
	{
		for (int j = i; j < i + n; ++j)
		{
			if (j != i + n - 1)		phrase += words[j] + ' ';
			else					phrase += words[j];
		}
		++phrases[phrase];
		phrase = "";
	}
	return phrases;
}

vector<pair<int, string>> sort_phrases(int m, const map<string, int>& phrases)
{
	vector<pair<int, string>> sorted;

	for (auto i : phrases)
	{
		if (i.second >= m)
		{
			sorted.push_back(make_pair(i.second, i.first));
		}
	}
	sort(sorted.rbegin(), sorted.rend());

	return sorted;
}