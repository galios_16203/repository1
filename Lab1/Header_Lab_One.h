#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <string>
#include <map>
#include <algorithm>

std::vector<std::string> get_vector(std::istream& istream);
std::map <std::string, int> get_phrases(int phrase_len, const std::vector <std::string>& words);
std::vector <std::pair<int, std::string>> sort_phrases(int min_freq, const std::map <std::string, int>& phrases);