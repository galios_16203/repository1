#include "Date.h"

Date::Date()
{
	time_t raw;
	tm *now;

	time(&raw);
	now = localtime(&raw);
	y = now->tm_year + 1900;
	mon = static_cast<Month>(now->tm_mon);
	d = now->tm_mday;
	h = now->tm_hour;
	min = now->tm_min;
	sec = now->tm_sec;
}

Date::Date(int hour, int minute, int second)
	: h{ hour }, min{ minute }, sec{ second }
{
	time_t raw;
	tm *now;

	time(&raw);
	now = localtime(&raw);
	y = now->tm_year + 1900;
	mon = static_cast<Month>(now->tm_mon);
	d = now->tm_mday;

	if (!is_date()) normalize();
}

Date Date::addYears(int n)
{
	Date date(*this);

	date.y += n;
	if (!date.is_date()) date.normalize();
	return date;
}

Date Date::addMonths(int n)
{
	Date date(*this);

	while (n > 0)
	{
		n--;
		date.month_cycle(1);
	}
	while (n < 0)
	{
		n++;
		date.month_cycle(0);
	}
	if (!date.is_date()) date.normalize();
	return date;
}

Date Date::addDays(int n)
{
	Date date(*this);

	date.d += n;
	if (!date.is_date()) date.normalize();
	return date;
}

Date Date::addHours(int n)
{
	Date date(*this);

	date.h += n;
	if (!date.is_date()) date.normalize();
	return date;
}

Date Date::addMinutes(int n)
{
	Date date(*this);

	date.min += n;
	if (!date.is_date()) date.normalize();
	return date;
}

Date Date::addSeconds(int n)
{
	Date date(*this);

	date.sec += n;
	if (!date.is_date()) date.normalize();
	return date;
}

Date & Date::operator=(const Date & date)
{
	y = date.year();
	mon = date.month();
	d = date.day();
	h = date.hour();
	min = date.minute();
	sec = date.second();
	return *this;
}

Date Date::operator+(const DateInterval & interval) const
{
	Date date(*this);
	date = date.addSeconds(interval.second());
	date = date.addMinutes(interval.minute());
	date = date.addHours(interval.hour());
	date = date.addDays(interval.day());
	date = date.addMonths(interval.month());
	date = date.addYears(interval.year());

	return date;
}

Date & Date::operator+=(const DateInterval & interval)
{
	*this = (*this).addSeconds(interval.second());
	*this = (*this).addMinutes(interval.minute());
	*this = (*this).addHours(interval.hour());
	*this = (*this).addDays(interval.day());
	*this = (*this).addMonths(interval.month());
	*this = (*this).addYears(interval.year());

	return *this;
}

Date Date::operator-(const DateInterval & interval) const
{
	Date date(*this);
	date = date.addSeconds(-interval.second());
	date = date.addMinutes(-interval.minute());
	date = date.addHours(-interval.hour());
	date = date.addDays(-interval.day());
	date = date.addYears(-interval.year());
	date = date.addMonths(-interval.month());

	return date;
}

Date & Date::operator-=(const DateInterval & interval)
{
	*this = (*this).addSeconds(-interval.second());
	*this = (*this).addMinutes(-interval.minute());
	*this = (*this).addHours(-interval.hour());
	*this = (*this).addDays(-interval.day());
	*this = (*this).addYears(-interval.year());
	*this = (*this).addMonths(-interval.month());

	return *this;
}

Date & Date::operator++()
{
	*this = (*this).addSeconds(1);
	return *this;
}

Date Date::operator++(int)
{
	Date date(*this);
	*this = (*this).addSeconds(1);
	return date;
}

Date & Date::operator--()
{
	*this = (*this).addSeconds(-1);
	return *this;
}

Date Date::operator--(int)
{
	Date date(*this);
	*this = (*this).addSeconds(-1);
	return date;
}

bool Date::operator==(const Date & date) const
{
	if ((y == date.year()) && (mon == date.month()) && (d == date.day()) && (h == date.hour()) && (min == date.minute()) && (sec == date.second())) return true;
	return false;
}

std::string Date::toString()
{
	std::string str;

	str = std::to_string(y) + "-" + month_to_string() + "-" + std::to_string(d) + " " + std::to_string(h) + "::" + std::to_string(min) + "::" + std::to_string(sec);
	return str;
}

std::string Date::formDate(std::string & format)
{
	std::size_t found[7];
	std::size_t min1 = std::string::npos;
	std::size_t min2 = 0;
	int what = 0;
	int flag = 1;
	std::string str;

	found[0] = format.find("YYYY");
	found[1] = format.find("MM");
	found[2] = format.find("MMM");
	found[3] = format.find("DD");
	found[4] = format.find("hh");
	found[5] = format.find("mm");
	found[6] = format.find("ss");

	if (found[1] == found[2]) found[1] = std::string::npos;
	if (found[0] == std::string::npos &&  found[1] == std::string::npos && found[2] == std::string::npos && found[3] == std::string::npos && found[4] == std::string::npos && found[5] == std::string::npos && found[6] == std::string::npos) str = "Invalid date format";

	while (flag)
	{
		for (int i = 0; i < 7; i++)
		{
			if (found[i] < min1)
			{
				min1 = found[i];
				what = i;
			}
		}

		if (min1 != std::string::npos)
		{
			str += format.substr(min2, min1 - min2);
			switch (what)
			{
			case 0:
			{
				str += std::to_string(y);
				min2 = min1 + 4;
				break;
			}
			case 1:
			{
				str += std::to_string(static_cast<int>(mon));
				min2 = min1 + 2;
				break;
			}
			case 2:
			{
				str += month_to_string();
				min2 = min1 + 3;
				break;
			}
			case 3:
			{
				str += std::to_string(d);
				min2 = min1 + 2;
				break;
			}
			case 4:
			{
				str += std::to_string(h);
				min2 = min1 + 2;
				break;
			}
			case 5:
			{
				str += std::to_string(min);
				min2 = min1 + 2;
				break;
			}
			case 6:
			{
				str += std::to_string(sec);
				min2 = min1 + 2;
				break;
			}
			}
			min1 = std::string::npos;
		}
		else
		{
			str += format.substr(min2);
			flag = 0;
		}
		found[what] = std::string::npos;
	}
	return str;
}

DateInterval Date::getInterval(const Date & another) const
{
	DateInterval interval{ another.y - y, another.month_int() - static_cast<int>(mon), another.d - d, another.h - h, another.min - min, another.sec - sec };

	return interval;
}

Date Date::addInterval(const DateInterval & interval) const
{
	return Date(y + interval.year(), static_cast<int>(mon) + interval.month(), d + interval.day(), h + interval.hour(), min + interval.minute(), sec + interval.second());
}

void Date::normalize()
{
	while (sec > 59)
	{
		min += 1;
		sec -= 60;
	}
	while (sec < 0)
	{
		min -= 1;
		sec += 60;
	}
	while (min > 59)
	{
		h += 1;
		min -= 60;
	}
	while (min < 0)
	{
		h -= 1;
		min += 60;
	}
	while (h > 23)
	{
		d += 1;
		h -= 24;
	}
	while (h < 0)
	{
		d -= 1;
		h += 24;
	}
	if (d > 28 || d < 1)	month_day_problem();
	if (y > 9999 || y < 1)
	{
		throw std::exception();
	}
}

bool Date::is_date()
{
	if (sec < 0 || sec > 59 || min < 0 || min > 59 || h < 0 || h > 23 || !is_day() || y > 9999 || y < 1) return false;
	return true;
}

DateInterval &DateInterval::operator=(const DateInterval & interval)
{
	y = interval.year();
	mon = interval.month();
	d = interval.day();
	h = interval.hour();
	min = interval.minute();
	sec = interval.second();
	return *this;
}

bool DateInterval::operator==(const DateInterval & interval) const
{
	if ((y == interval.year()) && (mon == interval.month()) && (d == interval.day()) && (h == interval.hour()) && (min == interval.minute()) && (sec == interval.second())) return true;
	return false;
}

void Date::month_day_problem()
{
	while (d < 1)
	{
		if (is_full_month())
		{
			d += 31;
			month_cycle(0);
		}
		else if (mon != Month::february)
		{
			d += 30;
			month_cycle(0);
		}
		else
		{
			if (is_leap())
			{
				d += 29;
				month_cycle(0);
			}
			else
			{
				d += 28;
				month_cycle(0);
			}
		}
	}
	while (d > 31)
	{
		if (is_full_month())
		{
			d -= 31;
			month_cycle(1);
		}
		else if (mon != Month::february)
		{
			d -= 30;
			month_cycle(1);
		}
		else
		{
			if (is_leap())
			{
				d -= 29;
				month_cycle(1);
			}
			else
			{
				d -= 28;
				month_cycle(1);
			}
		}
	}
	if (d > 28 && mon == Month::february && !is_leap())
	{
		d -= 28;
		month_cycle(1);
	}
	if (d > 29 && mon == Month::february && is_leap())
	{
		d -= 29;
		month_cycle(1);
	}
	if (d > 30 && !is_full_month())
	{
		d -= 30;
		month_cycle(1);
	}
	return;
}

bool Date::is_leap()
{
	if ((y % 4 == 0) && (y % 100 != 0) || (y % 400 == 0)) return true;
	else return false;
}

bool Date::is_day()
{
	if (d < 0 || d > 31 || ((d == 31) && !(is_full_month())) || ((d == 30) && (mon == Month::february)) || ((d == 29) && (mon == Month::february) && (!is_leap()))) return false;
	else return true;
}

bool Date::is_full_month()
{
	if (mon == Month::january || mon == Month::march || mon == Month::may || mon == Month::july || mon == Month::august || mon == Month::october || mon == Month::december) return true;
	else return false;
}

std::string Date::month_to_string()
{
	std::string str = "???";
	switch (mon)
	{
	case Month::january:
	{
		str = "Jan";
		break;
	}
	case Month::february:
	{
		str = "Feb";
		break;
	}
	case Month::march:
	{
		str = "Mar";
		break;
	}
	case Month::april:
	{
		str = "Apr";
		break;
	}
	case Month::may:
	{
		str = "May";
		break;
	}
	case Month::june:
	{
		str = "Jun";
		break;
	}
	case Month::july:
	{
		str = "Jul";
		break;
	}
	case Month::august:
	{
		str = "Aug";
		break;
	}
	case Month::september:
	{
		str = "Sep";
		break;
	}
	case Month::october:
	{
		str = "Oct";
		break;
	}
	case Month::november:
	{
		str = "Nov";
		break;
	}
	case Month::december:
	{
		str = "Dec";
		break;
	}
	default:
		break;
	}
	return str;
}

void Date::month_cycle(int way)
{
	if (way)
	{
		switch (mon)
		{
		case Month::january:
		{
			mon = Month::february;
			break;
		}
		case Month::february:
		{
			mon = Month::march;
			break;
		}
		case Month::march:
		{
			mon = Month::april;
			break;
		}
		case Month::april:
		{
			mon = Month::may;
			break;
		}
		case Month::may:
		{
			mon = Month::june;
			break;
		}
		case Month::june:
		{
			mon = Month::july;
			break;
		}
		case Month::july:
		{
			mon = Month::august;
			break;
		}
		case Month::august:
		{
			mon = Month::september;
			break;
		}
		case Month::september:
		{
			mon = Month::october;
			break;
		}
		case Month::october:
		{
			mon = Month::november;
			break;
		}
		case Month::november:
		{
			mon = Month::december;
			break;
		}
		case Month::december:
		{
			mon = Month::january;
			*this = (*this).addYears(1);
			break;
		}
		default:
			break;
		}
	}
	else
	{
		switch (mon)
		{
		case Month::january:
		{
			mon = Month::december;
			*this = (*this).addYears(-1);
			break;
		}
		case Month::february:
		{
			mon = Month::january;
			break;
		}
		case Month::march:
		{
			mon = Month::february;
			break;
		}
		case Month::april:
		{
			mon = Month::march;
			break;
		}
		case Month::may:
		{
			mon = Month::april;
			break;
		}
		case Month::june:
		{
			mon = Month::may;
			break;
		}
		case Month::july:
		{
			mon = Month::june;
			break;
		}
		case Month::august:
		{
			mon = Month::july;
			break;
		}
		case Month::september:
		{
			mon = Month::august;
			break;
		}
		case Month::october:
		{
			mon = Month::september;
			break;
		}
		case Month::november:
		{
			mon = Month::october;
			break;
		}
		case Month::december:
		{
			mon = Month::november;
			break;
		}
		default:
			break;
		}
	}

}