#define CATCH_CONFIG_RUNNER 
#include "Date.h" 
#include "Catch.h" 

TEST_CASE("Gets info from fields", "[field access]")
{
	Date date1{ 2000, 3, 21, 11, 23, 17 };
	Date date2{ 2, 3, 4, 5, 6, 7 };
	int year1 = 2000, year2 = 2, day1 = 21, day2 = 4, hour1 = 11, hour2 = 5, minute1 = 23, minute2 = 6, second1 = 17, second2 = 7;
	Month mon = Month::march;

	REQUIRE(date1.year() == year1);
	REQUIRE(date1.month() == mon);
	REQUIRE(date1.day() == day1);
	REQUIRE(date1.hour() == hour1);
	REQUIRE(date1.minute() == minute1);
	REQUIRE(date1.second() == second1);
	REQUIRE(date2.year() == year2);
	REQUIRE(date2.month() == mon);
	REQUIRE(date2.day() == day2);
	REQUIRE(date2.hour() == hour2);
	REQUIRE(date2.minute() == minute2);
	REQUIRE(date2.second() == second2);
}

TEST_CASE("Returns date plus n seconds", "[addSeconds]")
{
	int n1 = 1, n2 = 2, n3 = 3, m = -1;
	int sec1 = 59, sec2 = 0, sec3 = 1, sec4 = 57;
	
	Date date{ 2000, 1, 1, 1, 1, 58 };

	Date date1 = date.addSeconds(n1);
	Date date2 = date.addSeconds(n2);
	Date date3 = date.addSeconds(n3);
	Date date4 = date.addSeconds(m);

	REQUIRE(sec1 == date1.second());
	REQUIRE(sec2 == date2.second());
	REQUIRE(sec3 == date3.second());
	REQUIRE(sec4 == date4.second());
}

TEST_CASE("Returns date plus n minutes", "[addMinutes]")
{
	int n1 = 1, n2 = 2, n3 = 3, m = -1;
	int min1 = 59, min2 = 0, min3 = 1, min4 = 57;

	Date date{ 2000, 1, 1, 1, 58, 0 };

	Date date1 = date.addMinutes(n1);
	Date date2 = date.addMinutes(n2);
	Date date3 = date.addMinutes(n3);
	Date date4 = date.addMinutes(m);

	REQUIRE(min1 == date1.minute());
	REQUIRE(min2 == date2.minute());
	REQUIRE(min3 == date3.minute());
	REQUIRE(min4 == date4.minute());
}

TEST_CASE("Returns date plus n hours", "[addHours]")
{
	int n1 = 1, n2 = 2, n3 = 3, m = -1;
	int hour1 = 23, hour2 = 0, hour3 = 1, hour4 = 21;

	Date date{ 2000, 1, 1, 22, 0, 0 };

	Date date1 = date.addHours(n1);
	Date date2 = date.addHours(n2);
	Date date3 = date.addHours(n3);
	Date date4 = date.addHours(m);

	REQUIRE(hour1 == date1.hour());
	REQUIRE(hour2 == date2.hour());
	REQUIRE(hour3 == date3.hour());
	REQUIRE(hour4 == date4.hour());
}

TEST_CASE("Returns date plus n days", "[addDays]")
{
	int n1 = 1, n2 = 2, n3 = 3, m = -1;
	int day1 = 29, day2 = 1, day3 = 2, day4 = 27;

	Date date{ 2004, 2, 28, 1, 1, 0 };

	Date date1 = date.addDays(n1);
	Date date2 = date.addDays(n2);
	Date date3 = date.addDays(n3);
	Date date4 = date.addDays(m);

	REQUIRE(day1 == date1.day());
	REQUIRE(day2 == date2.day());
	REQUIRE(day3 == date3.day());
	REQUIRE(day4 == date4.day());
}

TEST_CASE("Returns date plus n months", "[addMonths]")
{
	int n1 = 1, n2 = 2, n3 = 3, m = -1;
	Date date{ 2000, 11, 1, 1, 1, 58 };

	Date date1 = date.addMonths(n1);
	Date date2 = date.addMonths(n2);
	Date date3 = date.addMonths(n3);
	Date date4 = date.addMonths(m);

	REQUIRE(Month::december == date1.month());
	REQUIRE(Month::january == date2.month());
	REQUIRE(Month::february == date3.month());
	REQUIRE(Month::october == date4.month());
}

TEST_CASE("Returns date plus n years", "[addYears]")
{
	int n1 = 1, n2 = 2, n3 = 3, m = -1;
	int year1 = 2000, year2 = 2001, year3 = 2002, year4 = 1998;

	Date date{ 1999, 1, 1, 1, 1, 58 };

	Date date1 = date.addYears(n1);
	Date date2 = date.addYears(n2);
	Date date3 = date.addYears(n3);
	Date date4 = date.addYears(m);

	REQUIRE(year1 == date1.year());
	REQUIRE(year2 == date2.year());
	REQUIRE(year3 == date3.year());
	REQUIRE(year4 == date4.year());
}

TEST_CASE("Operator =", "[operator =]")
{
	Date date{ 2000, 1, 1, 1, 1, 1 };
	Date date1 = date;	

	REQUIRE(date == date1);
}

TEST_CASE("Operator +", "[operator +]")
{
	Date date{ 2000, 1, 1, 1, 1, 1 }, date1{ 2000, 1, 2, 0, 0, 0 }, date2{ 2004, 2, 29, 1, 1, 1 }, date3{ 2003, 3, 1, 1, 1, 1 }, date4{ 1998, 11, 29, 23, 59, 59};
	DateInterval interval1{ 0, 0, 0, 22, 58, 59 }, interval2{ 4, 1, 28, 0, 0, 0 }, interval3{ 3, 1, 28, 0, 0, 0 }, interval4{-1, -1, -2, -1, -1, -2};
	
	Date dat1 = date + interval1;
	Date dat2 = date + interval2;
	Date dat3 = date + interval3;
	Date dat4 = date + interval4;

	REQUIRE(dat1 == date1);
	REQUIRE(dat2 == date2);
	REQUIRE(dat3 == date3);
	REQUIRE(dat4 == date4);
}

TEST_CASE("Operator +=", "[operator +=]")
{
	Date dat1{ 2000, 1, 1, 1, 1, 1 }, dat2{ 2000, 1, 1, 1, 1, 1 }, dat3{ 2000, 1, 1, 1, 1, 1 }, dat4{ 2000, 1, 1, 1, 1, 1 }, date1{ 2000, 1, 2, 0, 0, 0 }, date2{ 2004, 2, 29, 1, 1, 1 }, date3{ 2003, 3, 1, 1, 1, 1 }, date4{ 1998, 11, 29, 23, 59, 59 };
	DateInterval interval1{ 0, 0, 0, 22, 58, 59 }, interval2{ 4, 1, 28, 0, 0, 0 }, interval3{ 3, 1, 28, 0, 0, 0 }, interval4{ -1, -1, -2, -1, -1, -2 };

	dat1 += interval1;
	dat2 += interval2;
	dat3 += interval3;
	dat4 += interval4;

	REQUIRE(dat1 == date1);
	REQUIRE(dat2 == date2);
	REQUIRE(dat3 == date3);
	REQUIRE(dat4 == date4);
}

TEST_CASE("Operator -", "[operator -]")
{
	Date date{ 2000, 1, 1, 1, 1, 1 }, date1{ 1999, 12, 31, 23, 59, 59 }, date2{ 1996, 2, 29, 1, 1, 1 }, date3{ 1997, 3, 1, 1, 1, 1 }, date4{ 2001, 2, 3, 0, 0, 0 };
	DateInterval interval1{ 0, 0, 0, 1, 1, 2 }, interval2{ 3, 10, 3, 0, 0, 0 }, interval3{ 2, 10, 3, 0, 0, 0 }, interval4{ -1, -1, -1, -22, -58, -59 };

	Date dat1 = date - interval1;
	Date dat2 = date - interval2;
	Date dat3 = date - interval3;
	Date dat4 = date - interval4;

	REQUIRE(dat1 == date1);
	REQUIRE(dat2 == date2);
	REQUIRE(dat3 == date3);
	REQUIRE(dat4 == date4);
}

TEST_CASE("Operator -=", "[operator -=]")
{
	Date dat1{ 2000, 1, 1, 1, 1, 1 }, dat2{ 2000, 1, 1, 1, 1, 1 }, dat3{ 2000, 1, 1, 1, 1, 1 }, dat4{ 2000, 1, 1, 1, 1, 1 }, date1{ 1999, 12, 31, 23, 59, 59 }, date2{ 1996, 2, 29, 1, 1, 1 }, date3{ 1997, 3, 1, 1, 1, 1 }, date4{ 2001, 2, 3, 0, 0, 0 };
	DateInterval interval1{ 0, 0, 0, 1, 1, 2 }, interval2{ 3, 10, 3, 0, 0, 0 }, interval3{ 2, 10, 3, 0, 0, 0 }, interval4{ -1, -1, -1, -22, -58, -59 };

	dat1 -= interval1;
	dat2 -= interval2;
	dat3 -= interval3;
	dat4 -= interval4;

	REQUIRE(dat1 == date1);
	REQUIRE(dat2 == date2);
	REQUIRE(dat3 == date3);
	REQUIRE(dat4 == date4);
}

TEST_CASE("Adds 1 second and returns date", "[prefix plusplus operator]")
{
	Date date{ 1999, 12, 31, 23, 59 , 58}, dat{ 1999, 12, 31, 23, 59 , 59}, da{ 2000, 1, 1, 0, 0, 0};

	++date;
	REQUIRE(dat == date);
	REQUIRE(da == ++dat);
}

TEST_CASE("Returns date and adds 1 second", "[postfix plusplus operator ]")
{
	Date date{ 1999, 12, 31, 23, 59 , 59 }, dat{ 1999, 12, 31, 23, 59 , 59 }, da{ 2000, 1, 1, 0, 0, 0 }, d{ 1999, 12, 31, 23, 59 , 59 };
	
	REQUIRE(dat == date++);
	REQUIRE(da == date);
	REQUIRE(d == dat);
}

TEST_CASE("Subtracts 1 second and returns date", "[prefix minusminus operator]")
{
	Date date{ 1999, 12, 31, 23, 59 , 58 }, dat{ 1999, 12, 31, 23, 59 , 59 }, da{ 2000, 1, 1, 0, 0, 0 };

	--da;
	REQUIRE(da == dat);
	REQUIRE(date == --dat);
}

TEST_CASE("Returns date and subtracts 1 second", "[postfix minusminus operator]")
{
	Date date{ 1999, 12, 31, 23, 59 , 59 }, dat{ 2000, 1, 1, 0, 0, 0 }, da{ 2000, 1, 1, 0, 0, 0 };

	REQUIRE(da == dat--);
	REQUIRE(date == dat);
}

TEST_CASE("Operator ==", "[operator ==]")
{
	Date date{ 1999, 12, 31, 23, 59 , 59 }, dat{ 2000, 1, 1, 0, 0, 0 }, da{ 2000, 1, 1, 0, 0, 0 };

	REQUIRE(da == dat);
	REQUIRE(!(date == dat));
}

TEST_CASE("Turns date structure into string", "[toString]")
{
	Date date{ 1999, 12, 31, 23, 59 , 59 };
	std::string str = "1999-Dec-31 23::59::59";
	std::string strd = date.toString();

	REQUIRE(str == strd);
}

TEST_CASE("Turns date structure into format string", "[formatDate]")
{
	Date date{ 1999, 12, 31, 23, 59 , 59 };
	std::string str = "1999-Dec-31 23::59::59";
	std::string str1 = "The year is 1999, 12 month, 31 day";
	std::string format1 = "YYYY-MMM-DD hh::mm::ss";
	std::string format2 = "The year is YYYY, MM month, DD day";
	std::string str2 = date.formDate(format1);
	std::string str3 = date.formDate(format2);


	REQUIRE(str == str2);
	REQUIRE(str1 == str3);
}

TEST_CASE("Adds interval to date", "[addInterval]")
{
	Date date1{ 1999, 12, 31, 23, 59 , 59 }, date2{ 1999, 12, 31, 23, 59, 59 }, date3{ 2000, 1, 1, 0, 0, 0 };
	DateInterval int1{ 0, 0, 0, 0, 0, 0 }, int2{ 0, 0, 0, 0, 0, 1 }, int3{ 0, 0, 0, 0, 0, -1 };

	Date dat1 = date1.addInterval(int1);
	Date dat2 = date1.addInterval(int2);
	Date dat3 = date3.addInterval(int3);

	REQUIRE(date1 == dat1);
	REQUIRE(date3 == dat2);
	REQUIRE(date2 == dat3);
}

int main(int argc, char* const argv[])
{
	int result = Catch::Session().run(argc, argv);
	std::cin.get();
	return result;
}