#pragma once
#include <time.h>
#include <iostream>
#include <string>

enum class Month
{
	january = 1, february, march, april, may, june, july, august, september, october, november, december
};

class DateInterval
{
private:
	int y;
	int mon;
	int d;
	int h;
	int min;
	int sec;
public:
	DateInterval(int year, int month, int day, int hour, int minute, int second)
		: y{ year }, mon{ month }, d{ day }, h{ hour }, min{ minute }, sec{ second }
	{}
	DateInterval(const DateInterval & interval)
		: y{ interval.year() }, mon{ interval.month() }, d{ interval.day() }, h{ interval.hour() }, min{ interval.minute() }, sec{ interval.second() }
	{}

	int second() const { return sec; }
	int minute() const { return min; }
	int hour() const { return h; }
	int day() const { return d; }
	int month() const { return mon; }
	int year() const { return y; }
	DateInterval &operator = (const DateInterval & interval);
	bool operator==(const DateInterval & interval) const;

};

class Date
{
private:
	int y;
	Month mon;
	int d;
	int h;
	int min;
	int sec;

	int month_int() const { return static_cast<int>(mon); }
	void normalize();
	bool is_date();
	void month_day_problem();
	std::string month_to_string();
	bool is_leap();
	bool is_day();
	bool is_full_month();
	void month_cycle(int way);
public:
	Date();																															
	Date(int year, int month, int day, int hour, int minute, int second)
		: y{ year }, mon{ static_cast<Month>(month) }, d{ day }, h{ hour }, min{ minute }, sec{ second }
	{ if (!is_date()) normalize(); }

	Date(int year, Month m, int day)
		: y{ year }, mon{ m }, d{ day }, h{ 0 }, min{ 0 }, sec{ 0 }
	{ if (!is_date()) normalize(); }

	Date(int hours, int minutes, int seconds);

	~Date() {};

	Date(const Date & date)
		: y{ date.year() }, mon{ date.month() }, d{ date.day() }, h{ date.hour() }, min{ date.minute() }, sec{ date.second() }
	{}

	int second() const { return sec; }
	int minute() const { return min; }
	int hour() const { return h; }
	int day() const { return d; }
	Month month() const { return mon; }
	int year() const { return y; }
	Date addYears(int n);
	Date addMonths(int n);
	Date addDays(int n);
	Date addHours(int n);
	Date addMinutes(int n);
	Date addSeconds(int n);
	Date&  operator= (const Date & date);
	Date  operator+ (const DateInterval &) const;
	Date& operator+=(const DateInterval &);
	Date  operator- (const DateInterval &) const;
	Date& operator-=(const DateInterval &);
	Date& operator++(); // prefix increment; adds 1 sec
	Date  operator++(int); // postfix increment
	Date& operator--(); // prefix decrement
	Date  operator--(int); // postfix decrement
	bool operator==(const Date & date) const;
	std::string toString();									//����� � ������� YYYY-MMM- DD hh::mm::ss
	std::string formDate(std::string & format);				//��������� ������ ������� ��� ������������� ����
	DateInterval getInterval(const Date & another) const;	//�������� ����� ������
	Date addInterval(const DateInterval & ) const;			//��������� �������� � ����
};