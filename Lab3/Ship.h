#pragma once
#include <vector>
#include <string>
#include <iostream>
#include <ctime>

class AI
{
private:
	int a[10][10];
	int	b[10][10];
	enum conds { nothing = 0, ship, hit, kill, miss };
	int left3;
	int left2;
	unsigned int hit_shots;
	unsigned int shots;
	unsigned int last_shot;
	unsigned int game_moment;
	bool is_cell(const int pos) const
	{
		if (pos < 100 && pos >= 0) return true;
		return false;
	}
	bool is_possible(const int pos) const
	{
		int x = pos / 10, y = pos % 10;
		if (pos >= 0 && pos < 100 && !b[x][y]) return true;
		return false;
	}
	void change_cell(conds how, int which) 	// 0 - nothing is known, 1 - hit, 2  - kill, 3 - miss; which == i * 10 + j
	{
		b[which / 10][which % 10] = static_cast<unsigned int>(how);
	}
	void after_kill();
	void send_server(std::string message)
	{
		std::cout << message << std::endl;
	}
public:
	AI() 
	{
		left2 = 3;
		left3 = 2;
		for (int i = 0; i < 10; i++)
			for (int j = 0; j < 10; j++)
				a[i][j] = 0;
		for (int i = 0; i < 10; i++)
			for (int j = 0; j < 10; j++)
				b[i][j] = 0;
	}
	~AI() {}
	void arrange();
	unsigned int get_server();
	void strategy(int response); // 0 - shoot, 1 - miss, 2- hit, 3 - kill
	void finishing();
	void enemy_hit(std::string cell);
	void change_cells_kill(int cell);
};