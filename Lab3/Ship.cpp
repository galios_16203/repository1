#include "Ship.h"

void AI::arrange()
{
	srand(static_cast<unsigned int>(time(nullptr)));
	int dir1 = rand() % 2;
	int dir2 = rand() % 2;
	int i;

	if (dir1 == 0 && dir2 == 0)
	{
		for (i = 0; i < 4; i++)
			a[0][i] = ship;
		for (i = 5; i < 7; i++)
			a[0][i] = ship;
		for (i = 8; i < 10; i++)
			a[0][i] = ship;
		for (i = 0; i < 3; i++)
			a[2][i] = ship;
		for (i = 4; i < 7; i++)
			a[2][i] = ship;
		for (i = 8; i < 10; i++)
			a[2][i] = ship;
	}
	else if (dir1 == 0 && dir2 != 0)
	{
		for (i = 0; i < 4; i++)
			a[0][i] = ship;
		for (i = 5; i < 7; i++)
			a[0][i] = ship;
		for (i = 8; i < 10; i++)
			a[0][i] = ship;
		for (i = 2; i < 5; i++)
			a[i][0] = ship;
		for (i = 6; i < 9; i++)
			a[i][0] = ship;
		a[9][2] = ship;
		a[9][3] = ship;
	}
	else if (dir1 != 0 && dir2 == 0)
	{
		for (i = 0; i < 4; i++)
			a[i][0] = ship;
		for (i = 5; i < 7; i++)
			a[i][0] = ship;
		for (i = 8; i < 10; i++)
			a[i][0] = ship;
		for (i = 2; i < 5; i++)
			a[0][i] = ship;
		for (i = 6; i < 9; i++)
			a[0][i] = ship;
		a[2][9] = ship;
		a[3][9] = ship;
	}
	else
	{
		for (i = 0; i < 4; i++)
			a[i][0] = ship;
		for (i = 5; i < 7; i++)
			a[i][0] = ship;
		for (i = 8; i < 10; i++)
			a[i][0] = ship;
		for (i = 0; i < 3; i++)
			a[i][2] = ship;
		for (i = 4; i < 7; i++)
			a[i][2] = ship;
		for (i = 8; i < 10; i++)
			a[i][2] = ship;
	}

	i = 0;

	while (i < 4)
	{
		int pos = rand() % 100;
		if (!a[pos / 10][pos % 10] && !a[pos / 10 + 1][pos % 10] && !a[pos / 10 + 1][pos % 10 + 1] && !a[pos / 10 + 1][pos % 10 - 1] && !a[pos / 10 - 1][pos % 10 - 1] && !a[pos / 10 - 1][pos % 10 + 1] && !a[pos / 10 - 1][pos % 10] && !a[pos / 10][pos % 10 - 1] && !a[pos / 10][pos % 10 + 1])
		{
			a[pos / 10][pos % 10] = ship;
			i++;
		}
	}

	std::string str;
	for (i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			str += std::to_string(a[i][j]);
		}
		str += "\n";
	}
	send_server(str);
	game_moment = 0;
	shots = 3;
	hit_shots = 0;
	return;
}

unsigned int AI::get_server()
{
	std::string message;
	std::getline(std::cin, message);
	int found = message.find("Enemy shooted into");

	if (message == "Arrange!")
	{
		arrange();
		return 1;
	}
	else if (message == "Shoot!")
	{
		strategy(0);
		return 1;
	}
	else if (message == "Miss")
	{
		strategy(1);
		return 1;
	}
	else if (message == "Hit")
	{
		strategy(2);
		return 1;
	}
	else if (message == "Kill")
	{
		strategy(3);
		return 1;
	}
	else if (found != std::string::npos)
	{
		enemy_hit(message.substr(19));
		return 1;
	}
	else if (message == "Win!")
	{
		return 0;
	}
	else if (message == "Lose")
	{
		return 0;
	}
	else return 1;
}

void AI::strategy(int response)
{
	int x = 0, y = 0;
	std::string str = "";
	if (!response)
	{
		if (!hit_shots)
		{
			if (!game_moment)
			{
				while (!is_possible(shots))
				{
					if ((shots / 10) == ((shots + 4) / 10)) shots += 4;
					else if (shots % 10 == 9) shots += 1;
					else shots += 5;
				}
			}
			else if (game_moment == 1)
			{
				while (!is_possible(shots))
				{
					shots += 3;
				}

			}
			else if (game_moment == 2)
			{
				while (!is_possible(shots))
				{
					if ((shots / 10) == ((shots + 2) / 10)) shots += 2;
					else if (shots % 10 == 9) shots += 1;
					else shots += 3;
				}

			}
			else
			{
				shots = rand() % 100;
				while (!is_possible(shots))
				{
					shots++;
					if (shots >= 100)
						shots = shots % 100;
				}
			}
			str += static_cast<char>(65 + (shots / 10));
			str += " " + std::to_string(shots % 10);
			last_shot = shots;
			send_server(str);
		}
		else
		{
			finishing();
		}
	}
	else if (response == 1)
	{
		change_cell(miss, last_shot);
	}
	else if (response == 2)
	{
		change_cell(hit, last_shot);
		hit_shots = last_shot;
		finishing();
	}
	else if (response == 3)
	{
		after_kill();
		strategy(0);
	}
	return;
}

void AI::finishing()
{
	std::string str = "";
	if ((hit_shots / 10 == (hit_shots + 1) / 10) && b[hit_shots / 10][hit_shots % 10 + 1] == hit)
	{
		if ((hit_shots / 10 == (hit_shots - 1) / 10) && (b[hit_shots / 10][hit_shots % 10 - 1] == hit))
		{
			if ((hit_shots / 10 == (hit_shots - 2) / 10) && is_possible(hit_shots - 2))
			{
				str += static_cast<char>(65 + (hit_shots / 10));
				str += " " + std::to_string((hit_shots - 2) % 10);
				last_shot = hit_shots - 2;
				send_server(str);
			}
			else if ((hit_shots / 10 == (hit_shots + 2) / 10) && is_possible(hit_shots + 2))
			{
				str += static_cast<char>(65 + (hit_shots / 10));
				str += " " + std::to_string((hit_shots + 2) % 10);
				last_shot = hit_shots + 2;
				send_server(str);
			}
		}
		else if ((hit_shots / 10 == (hit_shots + 2) / 10) && b[hit_shots / 10][hit_shots % 10 + 2] == hit)
		{
			if ((hit_shots / 10 == (hit_shots - 1) / 10) && is_possible(hit_shots - 1))
			{
				str += static_cast<char>(65 + (hit_shots / 10));
				str += " " + std::to_string((hit_shots - 1) % 10);
				last_shot = hit_shots - 1;
				send_server(str);
			}
			else if ((hit_shots / 10 == (hit_shots + 3) / 10) && is_possible(hit_shots + 3))
			{
				str += static_cast<char>(65 + (hit_shots / 10));
				str += " " + std::to_string((hit_shots + 3) % 10);
				last_shot = hit_shots + 3;
				send_server(str);
			}
		}
		else
		{
			if ((hit_shots / 10 == (hit_shots - 1) / 10) && is_possible(hit_shots - 1))
			{
				str += static_cast<char>(65 + (hit_shots / 10));
				str += " " + std::to_string((hit_shots - 1) % 10);
				last_shot = hit_shots - 1;
				send_server(str);
			}
			else if ((hit_shots / 10 == (hit_shots + 2) / 10) && is_possible(hit_shots + 2))
			{
				str += static_cast<char>(65 + (hit_shots / 10));
				str += " " + std::to_string((hit_shots + 2) % 10);
				last_shot = hit_shots + 2;
				send_server(str);
			}
		}
	}
	else if ((hit_shots / 10 == (hit_shots - 1) / 10) && b[hit_shots / 10][hit_shots % 10 - 1] == hit)
	{
		if ((hit_shots / 10 == (hit_shots - 2) / 10) && (b[hit_shots / 10][hit_shots % 10 - 2] == hit))
		{
			if ((hit_shots / 10 == (hit_shots - 3) / 10) && is_possible(hit_shots - 3))
			{
				str += static_cast<char>(65 + (hit_shots / 10));
				str += " " + std::to_string((hit_shots - 3) % 10);
				last_shot = hit_shots - 3;
				send_server(str);
			}
			else if ((hit_shots / 10 == (hit_shots + 1) / 10) && is_possible(hit_shots + 1))
			{
				str += static_cast<char>(65 + (hit_shots / 10));
				str += " " + std::to_string((hit_shots + 1) % 10);
				last_shot = hit_shots + 1;
				send_server(str);
			}
		}
		else if ((hit_shots / 10 == (hit_shots + 1) / 10) && b[hit_shots / 10][hit_shots % 10 + 1] == hit)
		{
			if (is_possible(hit_shots - 2))
			{
				str += static_cast<char>(65 + (hit_shots / 10));
				str += " " + std::to_string((hit_shots - 2) % 10);
				last_shot = hit_shots - 2;
				send_server(str);
			}
			else if ((hit_shots / 10 == (hit_shots + 2) / 10) && is_possible(hit_shots + 2))
			{
				str += static_cast<char>(65 + (hit_shots / 10));
				str += " " + std::to_string((hit_shots + 2) % 10);
				last_shot = hit_shots + 2;
				send_server(str);
			}
		}
		else
		{
			if ((hit_shots / 10 == (hit_shots - 2) / 10) && is_possible(hit_shots - 2))
			{
				str += static_cast<char>(65 + (hit_shots / 10));
				str += " " + std::to_string((hit_shots - 2) % 10);
				last_shot = hit_shots - 2;
				send_server(str);
			}
			else if ((hit_shots / 10 == (hit_shots + 1) / 10) && is_possible(hit_shots + 1))
			{
				str += static_cast<char>(65 + (hit_shots / 10));
				str += " " + std::to_string((hit_shots + 1) % 10);
				last_shot = hit_shots + 1;
				send_server(str);
			}
		}
	}
	else if (is_cell(hit_shots + 10) && b[hit_shots / 10 + 1][hit_shots % 10] == hit)
	{
		if (is_cell(hit_shots + 20) && b[hit_shots / 10 + 2][hit_shots % 10] == hit)
		{
			if (is_possible(hit_shots - 10))
			{
				str += static_cast<char>(65 + ((hit_shots - 10) / 10));
				str += " " + std::to_string(hit_shots % 10);
				last_shot = hit_shots - 10;
				send_server(str);
			}
			else if (is_possible(hit_shots + 30))
			{
				str += static_cast<char>(65 + ((hit_shots + 30) / 10));
				str += " " + std::to_string(hit_shots % 10);
				last_shot = hit_shots + 30;
				send_server(str);
			}
		}
		else if (is_cell(hit_shots - 10) && (b[hit_shots / 10 - 1][hit_shots % 10] == hit))
		{
			if (is_possible(hit_shots - 20))
			{
				str += static_cast<char>(65 + ((hit_shots - 20) / 10));
				str += " " + std::to_string(hit_shots % 10);
				last_shot = hit_shots - 20;
				send_server(str);
			}
			else if (is_possible(hit_shots + 20))
			{
				str += static_cast<char>(65 + ((hit_shots + 20) / 10));
				str += " " + std::to_string(hit_shots % 10);
				last_shot = hit_shots + 20;
				send_server(str);
			}
		}
		else
		{
			if (is_possible(hit_shots - 10))
			{
				str += static_cast<char>(65 + ((hit_shots - 10) / 10));
				str += " " + std::to_string(hit_shots % 10);
				last_shot = hit_shots - 10;
				send_server(str);
			}
			else if (is_possible(hit_shots + 20))
			{
				str += static_cast<char>(65 + ((hit_shots + 20) / 10));
				str += " " + std::to_string(hit_shots % 10);
				last_shot = hit_shots + 20;
				send_server(str);
			}
		}
	}
	else if (is_cell(hit_shots - 10) && b[hit_shots / 10 - 1][hit_shots % 10] == hit)
	{
		if (is_cell(hit_shots + 10) && b[hit_shots / 10 + 1][hit_shots % 10] == hit)
		{
			if (is_possible(hit_shots + 10))
			{
				str += static_cast<char>(65 + ((hit_shots + 10) / 10));
				str += +" " + std::to_string(hit_shots % 10);
				last_shot = hit_shots + 10;
				send_server(str);
			}
			else if (is_possible(hit_shots - 20))
			{
				str += static_cast<char>(65 + ((hit_shots - 20) / 10));
				str += " " + std::to_string(hit_shots % 10);
				last_shot = hit_shots - 20;
				send_server(str);
			}
		}
		else if (is_cell(hit_shots - 20) && (b[hit_shots / 10 - 2][hit_shots % 10] == hit))
		{
			if (is_possible(hit_shots - 30))
			{
				str += static_cast<char>(65 + ((hit_shots - 30) / 10));
				str += " " + std::to_string(hit_shots % 10);
				last_shot = hit_shots - 30;
				send_server(str);
			}
			else if (is_possible(hit_shots + 10))
			{
				str += static_cast<char>(65 + ((hit_shots + 10) / 10));
				str += " " + std::to_string(hit_shots % 10);
				last_shot = hit_shots + 10;
				send_server(str);
			}
		}
		else
		{
			if (is_possible(hit_shots + 10))
			{
				str += static_cast<char>(65 + ((hit_shots + 10) / 10));
				str += " " + std::to_string(hit_shots % 10);
				last_shot = hit_shots + 10;
				send_server(str);
			}
			else if (is_possible(hit_shots - 20))
			{
				str += static_cast<char>(65 + ((hit_shots - 20) / 10));
				str += " " + std::to_string(hit_shots % 10);
				last_shot = hit_shots - 20;
				send_server(str);
			}
		}
	}
	else
	{
		if ((hit_shots / 10 == (hit_shots - 1) / 10) && is_possible(hit_shots - 1))
		{
			str += static_cast<char>(65 + (hit_shots / 10));
			str += " " + std::to_string((hit_shots - 1) % 10);
			last_shot = hit_shots - 1;
			send_server(str);
		}
		else if ((hit_shots / 10 == (hit_shots + 1) / 10) && is_possible(hit_shots + 1))
		{
			str += static_cast<char>(65 + (hit_shots / 10));
			str += " " + std::to_string((hit_shots + 1) % 10);
			last_shot = hit_shots + 1;
			send_server(str);
		}
		else if (is_possible(hit_shots + 10))
		{
			str += static_cast<char>(65 + ((hit_shots + 10) / 10));
			str += " " + std::to_string(hit_shots % 10);
			last_shot = hit_shots + 10;
			send_server(str);
		}
		else if (is_possible(hit_shots - 10))
		{
			str += static_cast<char>(65 + ((hit_shots - 10) / 10));
			str += " " + std::to_string(hit_shots % 10);
			last_shot = hit_shots - 10;
			send_server(str);
		}
	}
}

void AI::enemy_hit(std::string cell)
{
	int x, y;
	x = static_cast<int>(cell[0]) - 65;
	y = static_cast<int>(cell[2] - 48);
	a[x][y] = hit;
}

void AI::change_cells_kill(int cell)
{

	change_cell(kill, cell);
	if (is_cell(cell + 10))
	{
		change_cell(kill, cell + 10);
		if (is_cell(cell - 1) && (cell / 10 == (cell - 1) / 10))
		{
			change_cell(kill, cell - 1);
			if (is_cell(cell - 11))
				change_cell(kill, cell - 11);
		}
		if (is_cell(cell + 1) && (cell / 10 == (cell + 1) / 10))
		{
			change_cell(kill, cell + 1);
			if (is_cell(cell + 11))
				change_cell(kill, cell + 11);
		}
	}
	if (is_cell(cell - 10))
	{
		change_cell(kill, cell - 10);
		if (is_cell(cell - 1) && (cell / 10 == (cell - 1) / 10))
		{
			change_cell(kill, cell - 1);
			if (is_cell(cell - 11))
				change_cell(kill, cell - 11);
		}
		if (is_cell(cell + 1) && (cell / 10 == (cell + 1) / 10))
		{
			change_cell(kill, cell + 1);
			if (is_cell(cell - 9))
				change_cell(kill, cell - 9);
		}
	}
	if (is_cell(cell - 1) && (cell / 10 == (cell - 1) / 10))
	{
		change_cell(kill, cell - 1);
		if (is_cell(cell - 10))
		{
			change_cell(kill, cell - 10);
			if (is_cell(cell - 11))
				change_cell(kill, cell - 11);
		}
		if (is_cell(cell + 10))
		{
			change_cell(kill, cell + 10);
			if (is_cell(cell + 9))
				change_cell(kill, cell + 9);
		}
	}
	if (is_cell(cell + 1) && (cell / 10 == (cell + 1) / 10))
	{
		change_cell(kill, cell + 1);
		if (is_cell(cell - 10))
		{
			change_cell(kill, cell - 10);
			if (is_cell(cell - 9))
				change_cell(kill, cell - 9);
		}
		if (is_cell(cell + 10))
		{
			change_cell(kill, cell + 10);
			if (is_cell(cell + 11))
				change_cell(kill, cell + 11);
		}
	}
}

void AI::after_kill()
{
	int cell[3] = {-1, -1, -1};
	if (is_cell(last_shot + 10) && b[last_shot / 10 + 1][last_shot % 10] == hit)
	{
		cell[0] = last_shot + 10;
		if (is_cell(last_shot + 20) && b[last_shot / 10 + 2][last_shot % 10] == hit)
		{
			cell[1] = last_shot + 20;
			if (is_cell(last_shot + 30) && b[last_shot / 10 + 3][last_shot % 10] == hit)
			{
				cell[2] = last_shot + 30;
			}
			else if (is_cell(last_shot - 10) && b[last_shot / 10 - 1][last_shot % 10] == hit)
			{
				cell[2] = last_shot - 10;
			}
		}
		else if (is_cell(last_shot - 10) && b[last_shot / 10 - 1][last_shot % 10] == hit)
		{
			cell[1] = last_shot - 10;
			if (is_cell(last_shot - 20) && b[last_shot / 10 - 2][last_shot % 10] == hit)
			{
				cell[2] = last_shot - 20;
			}
		}
	}
	else if (is_cell(last_shot - 10) && b[last_shot / 10 - 1][last_shot % 10] == hit)
	{
		cell[0] = last_shot - 10;
		if (is_cell(last_shot - 20) && b[last_shot / 10 - 2][last_shot % 10] == hit)
		{
			cell[1] = last_shot - 20;
			if (is_cell(last_shot - 30) && b[last_shot / 10 - 3][last_shot % 10] == hit)
			{
				cell[2] = last_shot - 30;
			}
		}
	}
	else if (is_cell(last_shot + 1) && (last_shot / 10 == (last_shot + 1) / 10) && b[last_shot / 10][last_shot % 10 + 1] == hit)
	{
		cell[0] = last_shot + 1;
		if (is_cell(last_shot + 2) && (last_shot / 10 == (last_shot + 2) / 10) && b[last_shot / 10][last_shot % 10 + 2] == hit)
		{
			cell[1] = last_shot + 2;
			if (is_cell(last_shot + 3) && (last_shot / 10 == (last_shot + 3) / 10) && b[last_shot / 10][last_shot % 10 + 3] == hit)
			{
				cell[2] = last_shot + 3;
			}
			else if (is_cell(last_shot - 1) && (last_shot / 10 == (last_shot - 1) / 10) && b[last_shot / 10][last_shot % 10 - 1] == hit)
			{
				cell[2] = last_shot - 1;
			}
		}
		else if (is_cell(last_shot - 1) && (last_shot / 10 == (last_shot - 1) / 10) && b[last_shot / 10][last_shot % 10 - 1] == hit)
		{
			cell[1] = last_shot - 1;
			if (is_cell(last_shot - 2) && (last_shot / 10 == (last_shot - 2) / 10) && b[last_shot / 10][last_shot % 10 - 2] == hit)
			{
				cell[2] = last_shot - 2;
			}
		}
	}
	else if (is_cell(last_shot - 1) && (last_shot / 10 == (last_shot - 1) / 10) && b[last_shot / 10][last_shot % 10 - 1] == hit)
	{
		cell[0] = last_shot - 1;
		if (is_cell(last_shot - 2) && (last_shot / 10 == (last_shot - 2) / 10) && b[last_shot / 10][last_shot % 10 - 2] == hit)
		{
			cell[1] = last_shot - 2;
			if (is_cell(last_shot - 3) && (last_shot / 10 == (last_shot - 3) / 10) && b[last_shot / 10][last_shot % 10 - 3] == hit)
			{
				cell[2] = last_shot - 30;
			}
		}
	}

	change_cells_kill(last_shot);
	
	int count = 0;
	for (int i = 0; i < 3; i++)
		if (cell[i] != -1)
		{
			change_cells_kill(cell[i]);
			count++;
		}
	if (count == 3)
	{
		game_moment++;
		shots = 2;
	}
	else if (left3 == 1 && count == 2)
	{
		if (game_moment == 1)
		{
			game_moment++;
			shots = 1;
		}
		left3--;
	}
	else if (left3 == 2 && count == 2)	left3--;
	else if (left2 == 1 && count == 1)
	{
		if (game_moment == 2)	game_moment++;
		left2--;
	}
	else if (count == 1 && left2 > 1)	left2--;
	if (game_moment == 1 && left3 == 0)
	{
		game_moment++;
		shots = 1;
	}
	if (game_moment == 2 && left2 == 0) game_moment++;
	hit_shots = 0;
}


int main(int argc, char *argv[])
{
	AI player;
	int flag = 1;

	while (flag)
	{
		flag = player.get_server();
	}
	return 0;
}