Convey's Game of Life

This programm supports commands:

reset - clears a field and generation counter;
set XY (X - A to J, Y - 0 to 9) - makes cell alive;
clear XY - clears cell;
step N (also can be used without N) - display N generations on screen;
back - step 1 generation back
save "filename" - saves field as text file (without geenration history)
load "filename" - loads filed from text file (generation counter sets to zero)
