#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>

class Cell
{
private:
	bool cond;
public:
	Cell()
	{
		cond = false;
	}
	~Cell() {}
	bool state() const
	{
		return cond;
	};
	void change_state()
	{
		cond = !cond;
	}
};

class Field
{
private:
	Cell a[10][10];
public:
	Field() {};
	~Field() {};
	int count_cell(int pos);
	Field check_cells();
	bool state(int pos) const;
	void change_cell(int pos);
	std::string string_field();
	bool operator==(const Field & otrer);
};

class Generation
{
private:
	Field pres;
	int gen;
	Generation *prev;
	Generation *next;
public:
	Generation();
	~Generation();
	int get_gen() const
	{
		return gen;
	}
	Field get_field() const
	{
		return pres;
	}
	friend std::ostream& operator<<(std::ostream& stream, const Generation &gen);
	int get_message();
	int set_cell(std::string str, int mode);
	int next_gen();
	int prev_gen();
	int save(std::string filename);
	int load(std::string filename);
	void reset();
};