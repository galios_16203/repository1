#include "Field.h"

int Field::count_cell(int pos)
{
	int count = 0;
	int x = pos / 10, y = pos % 10;

	if (a[x][(y + 1) % 10].state() == true) count++;
	if (a[x][(y + 9) % 10].state() == true) count++; // x + 9 mod 10 == x - 1 mod 10
	if (a[(x + 1) % 10][y].state() == true) count++;
	if (a[(x + 9) % 10][y].state() == true) count++;
	if (a[(x + 1) % 10][(y + 1) % 10].state() == true) count++;
	if (a[(x + 1) % 10][(y + 9) % 10].state() == true) count++;
	if (a[(x + 9) % 10][(y + 1) % 10].state() == true) count++;
	if (a[(x + 9) % 10][(y + 9) % 10].state() == true) count++;
	
	return count;
}

Field Field::check_cells()
{
	Field next = *this;

	for (int i = 0; i < 100; i++)
	{
		int count = count_cell(i);
		if (a[i / 10][i % 10].state() == true)
		{
			if (count > 3 || count < 2) next.a[i / 10][i % 10].change_state();
		}
		else
		{
			if (count == 3) next.a[i / 10][i % 10].change_state();
		}
	}

	return next;
}

bool Field::state(int pos) const
{
	if (a[pos / 10][pos % 10].state()) return true;
	return false;
}

void Field::change_cell(int pos)
{
	a[pos / 10][pos % 10].change_state();
}

std::string Field::string_field()
{
	std::string str = "";

	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			if (a[i][j].state() == false) str += ".";
			else str += "*";
		}
		str += "\n";
	}

	return str;
}

bool Field::operator==(const Field & other)
{
	for (int i = 0; i < 100; i++)
	{
		if (this->state(i) != other.state(i)) return false;
	}

	return true;
}

std::ostream& operator<<(std::ostream& stream, const Generation &gen)
{
	Field f = gen.get_field();
	stream << f.string_field();
	return stream;
}

Generation::Generation()
{
	this->prev = nullptr;
	this->gen = 0;
}

Generation::~Generation()
{
}

int Generation::get_message()
{
	std::string message = "";
	std::getline(std::cin, message);

	int found[5];
	found[0] = message.find("set");
	found[1] = message.find("clear");
	found[2] = message.find("step");
	found[3] = message.find("save");
	found[4] = message.find("load");
	int r;

	if (message == "reset")
	{
		reset();
		std::cout << "Generation: " << this->get_gen() << std::endl;
		std::cout << *this << std::endl;
	}
	else if (found[0] != std::string::npos)
	{
		if (message[3] == ' ')
		{
			r = set_cell(message.substr(4), 1);
			if (r) std::cout << "This cell does not exist" << std::endl;
		}
		else
		{
			std::cout << "Unknown command" << std::endl;
		}
	}
	else if (found[1] != std::string::npos)
	{
		if (message[5] == ' ')
		{
			r = set_cell(message.substr(6), 0);
			if (r) std::cout << "This cell does not exist" << std::endl;
		}
		else
		{
			std::cout << "Unknown command" << std::endl;
		}
	}
	else if (found[2] != std::string::npos)
	{
		int n = 0, len = message.length();
		if (len != 4)
		{
			if (message[4] == ' ')
			{
				for (int i = 5; i < len; i++)
				{
					n *= 10;
					int k = static_cast<int>(message[i] - 48);
					if (k >= 0 && k < 10)
						n += k;
					else
					{
						std::cout << "Wrong symbol" << std::endl;
						return 0;
					}
				}
			}
			else
			{
				std::cout << "Unknown command" << std::endl;
			}
		}
		else n = 1;
		for (int i = 0; i < n; i++)
		{
			r = next_gen();
			if (r == 1)
			{
				std::cout << "Game is over" << std::endl;
				return 1;
			}
		}
	}
	else if (message == "back")
	{
		r = prev_gen();
		if (r == 1)
		{
			std::cout << "No previous generation" << std::endl;
		}
	}
	else if (found[3] != std::string::npos)
	{
		if (message[4] == ' ')
		{
			r = save(message.substr(5));
			if (r)	std::cout << "Can't open the file" << std::endl;
			else	std::cout << "Game saved" << std::endl;
		}
		else
		{
			std::cout << "Unknown command" << std::endl;
		}
	}
	else if (found[4] != std::string::npos)
	{
		if (message[4] == ' ')
		{
			r = load(message.substr(5));
			if (r == 1)
			{
				std::cout << "Can't open the file" << std::endl;
			}
			else if (r == 2)
			{
				std::cout << "Can't load the field" << std::endl;
			}
		}
		else
		{
			std::cout << "Unknown command" << std::endl;
		}
	}
	else
	{
		std::cout << "Unknown command" << std::endl;
	}
	return 0;
}

int Generation::set_cell(std::string str, int mode)
{
	int x, y;
	x = static_cast<int>(str[0]) - 65;
	y = static_cast<int>(str[1] - 48);
	if ((x * 10 + y >= 100) || (x > 10) || (x < 0) || (y > 10) || (y < 0)) return 1;
	if (mode)
	{
		if (!pres.state(x * 10 + y))
		{
			pres.change_cell(x * 10 + y);
		}
	}
	else
	{
		if (pres.state(x * 10 + y))
		{
			pres.change_cell(x * 10 + y);
		}
	}
	std::cout << "Generation: " << this->get_gen() << std::endl;
	std::cout << *this << std::endl;
	return 0;
}

int Generation::next_gen()
{
	int count = 0;
	Generation *temp = new Generation;
	temp->pres = pres;
	temp->prev = this->prev;
	temp->gen = gen;
	temp->next = this;
	gen++;
	this->prev = temp;
	pres = pres.check_cells();
	if (temp->pres == pres || count == 100)
	{
		return 1;
	}
	std::cout << "Generation: " << this->get_gen() << std::endl;
	std::cout << *this << std::endl;;
	return 0;
}

int Generation::prev_gen()
{
	if (this->prev == nullptr) return 1;
	else
	{
		this->gen = this->prev->get_gen();
		this->pres = this->prev->get_field();
		this->prev = this->prev->prev;
		std::cout << "Generation: " << this->get_gen() << std::endl;
		std::cout << *this << std::endl;
	}

	return 0;
}

int Generation::save(std::string filename)
{
	std::ofstream file{ filename };
	if (!file) return 1;

	file << *this;
	file.close();
	return 0;
}

int Generation::load(std::string filename)
{
	std::ifstream file{ filename };
	if (!file) return 1;

	std::string str = "", field = "";
	for (int i = 0; i < 10; i++)
	{
		std::getline(file, str);
		field += str;
	}

	Field saved;
	for (int i = 0; i < 100; i++)
	{
		if (field[i] == '*')
		{
			saved.change_cell(i);
		}
		else if (field[i] == '.') {}
		else return 2;
	}

	reset();
	this->pres = saved;
	std::cout << "Generation: " << this->get_gen() << std::endl;
	std::cout << *this << std::endl;
	return 0;
}

void Generation::reset()
{
	while (this->prev != nullptr)
	{
		delete (this->next);
		this->next = nullptr;
		*this = *this->prev;
		this->next->prev = nullptr;
	}
	this->next = nullptr;
	this->gen = 0;
	Field empty;
	this->pres = empty;

	return;
}

int main()
{
	Generation game;
	int flag = 0;
	std::cout << "Generation: " << game.get_gen() << std::endl;
	std::cout << game << std::endl;

	while (!flag)
	{
		flag = game.get_message();
	}
	return 0;
}